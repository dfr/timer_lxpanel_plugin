/*
 * ACPI battery monitor plugin for LXPanel
 *
 * Copyright (C) 2007 by Greg McNew <gmcnew@gmail.com>
 * Copyright (C) 2008 by Hong Jen Yee <pcman.tw@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * This plugin monitors battery usage on ACPI-enabled systems by reading the
 * battery information found in /proc/acpi/battery/. The update interval is
 * user-configurable and defaults to 1 second.
 *
 * The battery's remaining life is estimated from its current charge and current
 * rate of discharge. The user may configure an alarm command to be run when
 * their estimated remaining battery life reaches a certain level.
 */

/* FIXME:
 *  Here are somethings need to be improvec:
 *  1. Replace pthread stuff with gthread counterparts for portability.
 *  2. Check "/proc/acpi/ac_adapter" for AC power.
 *  3. Add an option to hide the plugin when AC power is used or there is no battery.
 *  4. Handle failure gracefully under systems other than Linux.
*/

#include <glib.h>
#include <glib/gi18n.h>
#include <pthread.h> /* used by pthread_create() and alarmThread */
#include <semaphore.h> /* used by update() and alarmProcess() for alarms */
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <lxpanel/plugin.h>

#define MINUTES_STR "minutes"

typedef struct {
    GtkWidget *infoText;
    unsigned int alarmTime,
        timeout_seconds,
        elapsed_seconds,
        timer;
} timerapp;


static void destructor(Plugin *p);
/*static void update_display(timerapp *b, gboolean repaint);*/


/* FIXME:
   Don't repaint if percentage of remaining charge and remaining time aren't changed. */
void update_display(timerapp *b) {

    char text[ 20 ];
    if(b->elapsed_seconds > 0) {
        g_snprintf( text, 20, "%02i:%02i", b->elapsed_seconds / 60 , b->elapsed_seconds % 60 );
    }
    else {
        g_snprintf( text, 20, "- -" );
    }
    gtk_label_set_text(b->infoText, text);

}

static void display_alert(gchar * message) {
    GtkWidget *dialog, *label, *content_area;
    dialog = gtk_dialog_new_with_buttons (_("Time is up!"),
            NULL,
            GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
            GTK_STOCK_OK, GTK_RESPONSE_NONE,
            NULL);
    label = gtk_label_new (message);
    content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));
    g_signal_connect_swapped (dialog,
            "response",
            G_CALLBACK (gtk_widget_destroy),
            dialog);
    gtk_container_add (GTK_CONTAINER (content_area), label);
    gtk_window_set_default_size(GTK_WINDOW (dialog), 360, 120);
    gtk_widget_show_all(dialog);
}

/* This callback is called every 1 seconds */
static int update_timout(timerapp *b) {
    /*GDK_THREADS_ENTER();*/


    /*GDK_THREADS_LEAVE();*/
    if( b->elapsed_seconds > 0 ) {
        --b->elapsed_seconds;
        if(b->elapsed_seconds <= 0) {
            b->elapsed_seconds = 0;
            display_alert(_("Time is up!"));
        }
    }
    update_display( b );
    return TRUE;
}


static void append_timer_time(GtkWidget * btn, timerapp *b) {
    int mins = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(btn), MINUTES_STR));
    b->elapsed_seconds += mins * 60;
    update_display( b );
}


static void g_list_append_to_menu(char * data, int minutes, GtkWidget* popup, timerapp *b)
{
    GtkWidget * mitem;
    mitem = gtk_image_menu_item_new_with_label ( data );
    g_object_set_data(G_OBJECT(mitem), MINUTES_STR, GINT_TO_POINTER(minutes));
    gtk_menu_shell_append (GTK_MENU_SHELL(popup), mitem);
    g_signal_connect(G_OBJECT(mitem),"activate", G_CALLBACK(append_timer_time), b);
}


/* An update will be performed whenever the user clicks on the charge bar */
static gint button_press_event(GtkWidget *widget, GdkEventButton *event,
        Plugin* plugin) {

    timerapp *b = (timerapp*)plugin->priv;
    char itemname[256];
    int i;

    /*update_display(b);*/

    if( event->button == 3 ) { /* right button */
        GtkMenu* popup = (GtkMenu*)lxpanel_get_panel_menu( plugin->panel, plugin, FALSE );
        gtk_menu_popup( popup, NULL, NULL, NULL, NULL, event->button, event->time );
        return TRUE;
    }
    if( event->button == 1 ) {  /* left button */
        GtkWidget * popup = gtk_menu_new();
        char * item_lbl = _("Add %i minutes");
        int minutes[] = { 1, 5, 10, 20, 30, 45, 60 };
        for (i = 0; i < 6; i++) {
            sprintf(itemname, item_lbl, minutes[i] );
            g_list_append_to_menu(itemname, minutes[i], popup, b);
        }
        gtk_widget_show_all(popup);
        gtk_menu_popup( (GtkMenu*)popup, NULL, NULL, NULL, NULL,
                        event->button, event->time );

        return TRUE;
    }
    return FALSE;
}



static int
constructor(Plugin *p, char **fp)
{
    timerapp *b;
    p->priv = b = g_new0(timerapp, 1);
    p->pwid = gtk_event_box_new();
    gtk_widget_set_has_window(p->pwid, FALSE);
    gtk_container_set_border_width( GTK_CONTAINER(p->pwid), 1 );


    char lbuf[10] = {'\0'};
    sprintf(lbuf, "- -");
    b->infoText = gtk_label_new(lbuf);
    gtk_container_add(GTK_CONTAINER(p->pwid), GTK_WIDGET(b->infoText));
    /*gtk_widget_set_size_request(p->pwid, 40, 25);*/

    gtk_widget_show_all(p->pwid);
    g_signal_connect (G_OBJECT (p->pwid), "button_press_event",
                    G_CALLBACK(button_press_event), (gpointer) p);

    /*if ((b->orientation = p->panel->orientation) == ORIENT_HORIZ) {*/
    /*gtk_widget_show(b->drawingArea);*/


    b->alarmTime = 5;
    b->elapsed_seconds = 0;
    b->timer = g_timeout_add_seconds( 1, (GSourceFunc) update_timout, (gpointer) b);
    return 1;
}


static void
destructor(Plugin *p)
{
    ENTER;

    timerapp *b = (timerapp *) p->priv;

    g_source_remove(b->timer);
    g_free(b);

    RET();

}


static void config(Plugin *p, GtkWindow* parent) {
    (void)p;
    (void)parent;
}


static void save(Plugin* p, FILE* fp) {
    (void)p;
    (void)fp;
}


PluginClass timerapp_plugin_class = {
    PLUGINCLASS_VERSIONING,

    type        : "timerapp",
    name        : N_("Alarm timer plugin"),
    description : N_("Emit alarm signals"),

    expand_available : FALSE,

    constructor : constructor,
    destructor  : destructor,
    config      : config,
    save        : save
};
